const path = require('path');
const express = require('express');
const hbs = require('hbs');

const app = express();

const publicDirectoryPath = path.join(__dirname, './public');
const viewsPath = path.join(__dirname, './templates/views');
const partialsPath = path.join(__dirname, './templates/partials');

app.set('view engine', 'hbs');
app.set('views', viewsPath);
hbs.registerPartials(partialsPath);

app.use(express.static(publicDirectoryPath));

app.get('', (req, res) => {
    res.render('', {
        title: 'Index title',
        subtitle: 'Index of handlebars partials',
    });
});

app.get('/about', (req, res) => {
    res.render('about', {
        title: 'About title',
        subtitle: 'About handlebars partials',
    });
});

app.get('/help', (req, res) => {
    res.render('help', {
        title: 'Help title',
        subtitle: 'Help section of the handlebars partials',
    });
});

app.get('*', (req, res) => {
    res.render('404', {
        title: 'Page Not found (status 404)',
        subtitle: 'Please navigate to another page from the menu',
    });
});

app.listen(3000, () => {
    console.log('Server is up on port 3000.');
});
