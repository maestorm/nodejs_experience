const request = require('postman-request');

const options = {
    url: 'https://sandbox-api.coinmarketcap.com/v1/cryptocurrency/categories?slug=shibainu&start=1',
    headers: {
        'X-CMC_PRO_API_KEY': '273bb01e-51ab-43ba-8ebc-b3058ad1275a',
    },
    json: true,
};

const result = () => request(options, (error, response, body) => {
    if (error) {
        throw error;
    } else {
        const { data } = body.data.shibainu;
        console.log(data);
    }
});

result();
